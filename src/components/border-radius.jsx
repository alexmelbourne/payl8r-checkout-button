import React, { useState } from "react";

const BorderRadius = () => {
  const [borderRadius, setborderRadius] = useState("");
  return (
    <label htmlFor="border-radius">
      Border Radius
      <input
        type="text"
        value={borderRadius}
        id="border-radius"
        onChange={event =>
          setborderRadius(
          document.documentElement.style.setProperty("--border-radius",`${event.target.value}px`)
          )
        }
      />
    </label>
  );
};

export default BorderRadius;
