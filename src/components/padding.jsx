import React, { useState } from "react";

const Padding = () => {
  const [padding, setpadding] = useState("");
  return (
    <label htmlFor="padding">
      padding
      <input
        type="text"
        value={padding}
        id="padding"
        onChange={event =>
          setpadding(
          document.documentElement.style.setProperty("--padding",`${event.target.value}px`)
          )
        }
      />
    </label>
  );
};

export default Padding;
