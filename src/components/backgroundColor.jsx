import React, { useState } from "react";

const BackgroundColor = () => {
  const [backgroundColor, setbackgroundColor] = useState("");
  return (
    <label htmlFor="backgroundColor">
      backgroundColor
      <input
        type="text"
        value={backgroundColor}
        id="backgroundColor"
        onChange={event =>
            setbackgroundColor(
          document.documentElement.style.setProperty("--bg-color",`${event.target.value}`)
          )
        }
      />
    </label>
  );
};

export default BackgroundColor;
