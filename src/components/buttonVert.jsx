import React from "react";

const MainButtonVert = ({ color }) => {
  return (
    <button
      className={`payl8r-checkout-button payl8r-checkout-button--${color}`}
    >
      Checkout
      <div className="payl8r-checkout-button__center">
        with
        <span
          className={`payl8r-checkout-button__logo ${
            color === "light" ? "payl8r-checkout-button__logo--darken" : ""
          }`}
        />
      </div>
    </button>
  );
};

export default MainButtonVert;
